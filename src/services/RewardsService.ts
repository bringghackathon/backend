import * as express from 'express';
import {Request, Response, Router} from "express";
import {handleErrors} from "./ServiceUtils";

const REWARDS = [
  {
    title: "GiftCard",
    subtitle: "Receive 50$ Giftcard",
    image_url: "https://thedollboutique.com/wp-content/uploads/2018/08/gift-card.jpg",
    number_of_points: 25
  },
  {
    title: "Free Gas",
    subtitle: "Receive 5 Gallons of Gas",
    image_url: "https://i.stack.imgur.com/CMo3S.jpg",
    number_of_points: 50
  },
  {
    title: "Family Meal",
    subtitle: "Receive a meal for 4 at Wendy's",
    image_url: "http://3.bp.blogspot.com/_bz-h5kgz1Zk/THxOhG-oyfI/AAAAAAAAAmY/_SUV014gz_0/s1600/OgAAAHwuNIFa0wv1jEGA_lhxm2syWSH7jIedf7CvdLrWDrS4p4jjsu3ve35SYv89_RSmY8Plw3Yr3rIE6SMv9AWeXX4Am1T1UOTbHBpWcW3yiPlVkm8EarpBc8_u.jpg",
    number_of_points: 75
  }
]

export default class RewardsService {
  private readonly router: Router;

  constructor() {
    const router = express.Router({mergeParams: true});

    router.get('/', handleErrors(this.getRewards.bind(this)));

    this.router = router;
  }

  private async getRewards(req: Request, res: Response) {
    res.json({
      rewards: REWARDS,
    });
  }

  getRoutes(): Router {
    return this.router;
  }
}