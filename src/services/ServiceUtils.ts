import {NextFunction, Request, RequestHandler, Response} from "express";

export function handleErrors(handler: RequestHandler): RequestHandler {
  return (req: Request, res: Response, next: NextFunction) => {
    return Promise
      .resolve(handler(req, res, next))
      .catch(next);
  };
}