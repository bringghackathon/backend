import * as express from 'express';
import {Request, Response, Router} from "express";
import {handleErrors} from "./ServiceUtils";
import {User} from "../entity/User";
import {UserNotification} from "../entity/UserNotification";

export default class NotificationsService {
  private readonly router: Router;

  constructor() {
    const router = express.Router({mergeParams: true});

    router.get('/', handleErrors(this.getNotifications.bind(this)));

    this.router = router;
  }

  private async getNotifications(req: Request, res: Response) {
    const user = req['user'] as User;

    const notifications = await UserNotification.findAllUnreadForUserId(user.bringg_user_id);
    const serializedNotifications = notifications.map((notification) => {
      const serializedNotification = notification.serialize();
      serializedNotification['mission']['parts_completed'] = serializedNotification['fulfilment']['parts_completed'];
      return serializedNotification;
    });

    await Promise.all(notifications.map(async (notification) => {
      await notification.markAsRead();
    }));

    res.json({
      notifications: serializedNotifications
    });
  }

  getRoutes(): Router {
    return this.router;
  }
}