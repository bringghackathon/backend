import {NextFunction, Request, Response} from "express";
import {ErrorWithCode} from "../../../shared/ErrorWithCode";
import {User} from "../../entity/User";

export async function authenticateUser(req: Request, res: Response, next: NextFunction): Promise<void> {
    if (req.method === "OPTIONS") {
      next();
      return;
    }

    try {
      const currentUser = await getUserForRequest(req);
      req['user'] = currentUser;
      next();
    } catch (error) {
      next(error);
    }
}

async function getUserForRequest(req: Request): Promise<User> {
  if (req['user'] instanceof User) {
    return req['user'];
  }

  const authenticationHeader = req.headers.authorization;
  if (!authenticationHeader) {
    throw new ErrorWithCode(`Missing authorization header`, 401, 14);
  }

  const bringgUserId = parseInt(authenticationHeader);
  return await getUserBringgUserId(bringgUserId);
}

function getUserBringgUserId(bringgUserId: number): Promise<User> {
  return User.getOrCreateUserByBringgUserId(bringgUserId);
}