import {NextFunction, Request, Response} from "express";
import {ErrorWithCode} from "../../../shared/ErrorWithCode";
import {logWarn} from "../../../shared/Logger";

export default function errorMiddleware(error: Error, req: Request, res: Response, next: NextFunction) {
  let httpCode = 500 || error['httpCode'];
  let errorCode = 0;
  let logMeta = {};
  if (error instanceof ErrorWithCode) {
    httpCode = error.httpCode;
    errorCode = error.errorCode || errorCode;
    logMeta = error.logMeta || {};
  }

  logWarn(error.message, {httpCode, errorCode, ...logMeta});
  res.status(httpCode).json({
    error:
      {message: error.message, errorCode: errorCode}
  });
}
