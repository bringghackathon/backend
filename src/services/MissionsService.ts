import * as express from 'express';
import {Request, Response, Router} from "express";
import {handleErrors} from "./ServiceUtils";
import {Mission} from "../entity/Mission";
import {User} from "../entity/User";
import {MissionFulfillment} from "../entity/MissionFulfillment";
import CreateMissionPayload = require("./models/CreateMissionPayload");
import UpdateMissionEnabledPayload = require("./models/UpdateMissionEnabledPayload");
import {ErrorWithCode} from "../../shared/ErrorWithCode";

export default class MissionsService {
  private readonly router: Router;

  constructor() {
    const router = express.Router({mergeParams: true});

    router.get('/', handleErrors(this.getMissions.bind(this)));
    router.post('/', handleErrors(this.createMission.bind(this)));
    router.post('/enabled', handleErrors(this.updateMissionEnabled.bind(this)));

    this.router = router;
  }

  private async createMission(req: Request, res: Response) {
    const createMissionPayload = new CreateMissionPayload(req.body);

    const createdMission = Mission.createFromData(createMissionPayload);
    await createdMission.save();

    res.json(createdMission.serialize());
  }

  private async updateMissionEnabled(req: Request, res: Response) {
    const updateMissionEnabled = new UpdateMissionEnabledPayload(req.body);

    const mission = await Mission.findOne(updateMissionEnabled.mission_id);
    if (!mission) {
      throw new ErrorWithCode("No message to update", 404, 238476, updateMissionEnabled);
    }

    mission.is_enabled = updateMissionEnabled.is_enabled;
    await mission.save();
    res.json(mission.serialize());
  }

  private async getMissions(req: Request, res: Response) {
    const user = req['user'] as User;

    const missions = await Mission.find();
    const missionsWithFulfillmentsPromises = missions.map(async (mission) => {
      const fulfillment = await MissionFulfillment.getOrCreateMissionFulfillment(user.bringg_user_id, mission.id);
      return {...mission.serialize(), ...fulfillment.serialize() }
    });

    const missionsWithFulfillments = await Promise.all(missionsWithFulfillmentsPromises);

    res.json({
      missions: missionsWithFulfillments,
    });
  }

  getRoutes(): Router {
    return this.router;
  }
}