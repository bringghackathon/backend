import * as express from 'express';
import {Request, Response, Router} from "express";
import {handleErrors} from "./ServiceUtils";
import {Mission} from "../entity/Mission";
import {User} from "../entity/User";
import {MissionFulfillment} from "../entity/MissionFulfillment";

const levels = [
  0,
  100,
  1000,
  5000,
  10000
]

function levelForNumberOfPoints(numberOfPoints: number): number {
  for (let index = 0; index < levels.length; index++) {
    const currentValue = levels[index];

    const nextValue = levels[index + 1];
    if (!nextValue) {
      return index + 1;
    }

    if (numberOfPoints >= currentValue && numberOfPoints < nextValue) {
      return index + 1;
    }
  }

  return levels.length;
}

export default class UserService {
  private readonly router: Router;

  constructor() {
    const router = express.Router({mergeParams: true});

    router.get('/', handleErrors(this.getMe.bind(this)));

    this.router = router;
  }

  private async getMe(req: Request, res: Response) {
    const user = req['user'] as User;

    const missions = await Mission.find();
    const missionsWithFulfillmentsPromises = missions.map(async (mission) => {
      const fulfillment = await MissionFulfillment.getOrCreateMissionFulfillment(user.bringg_user_id, mission.id);
      return {...mission.serialize(), ...fulfillment.serialize() }
    });

    const missionsWithFulfillments = await Promise.all(missionsWithFulfillmentsPromises);

    const point_sum = missionsWithFulfillments.reduce((previous, current) => {
      if (current.parts_completed >= current.achievement_parts) {
        return previous + current.point_reward;
      } else {
        return previous;
      }
    }, 0);

    const current_level = levelForNumberOfPoints(point_sum);
    const currentLevelIndex = current_level - 1;
    const nextLevelNumberOfPoints = levels[currentLevelIndex + 1];
    let points_to_next_level = 0;
    if (nextLevelNumberOfPoints) {
      points_to_next_level = nextLevelNumberOfPoints - point_sum;
    }

    res.json({point_sum, current_level, points_to_next_level});
  }

  getRoutes(): Router {
    return this.router;
  }
}