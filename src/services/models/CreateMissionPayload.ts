import * as ajv from 'ajv';
import * as fs from 'fs';
import {ErrorWithCode} from "../../../shared/ErrorWithCode";
import {MissionType} from "../../Enums/MissionType";

const schema = JSON.parse(fs.readFileSync(__dirname + '/schemas/CreateMissionPayload.json').toString());
const Ajv = new ajv();

class CreateMissionPayload {
  title: string;

  achievement_parts: number;

  point_reward: number;

  mission_type: MissionType;

  mission_parameter?: number;

  constructor(json: Partial<CreateMissionPayload>) {
    const valid = CreateMissionPayload.validate(json);
    if (!valid) {
      throw new ErrorWithCode(Ajv.errorsText(CreateMissionPayload.validate.errors), 400);
    }

    this.title = json.title;
    this.achievement_parts = json.achievement_parts;
    this.point_reward = json.point_reward;
    this.mission_type = json.mission_type;
    this.mission_parameter = json.mission_parameter;
  }

  private static validate = Ajv.compile(schema);
}

export = CreateMissionPayload;
