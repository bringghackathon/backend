import * as ajv from 'ajv';
import * as fs from 'fs';
import {ErrorWithCode} from "../../../shared/ErrorWithCode";

const schema = JSON.parse(fs.readFileSync(__dirname + '/schemas/UpdateMissionEnabledPayload.json').toString());
const Ajv = new ajv();

class UpdateMissionEnabledPayload {
  mission_id: string;

  is_enabled: boolean;

  constructor(json: Partial<UpdateMissionEnabledPayload>) {
    const valid = UpdateMissionEnabledPayload.validate(json);
    if (!valid) {
      throw new ErrorWithCode(Ajv.errorsText(UpdateMissionEnabledPayload.validate.errors), 400);
    }

    this.mission_id = json.mission_id;
    this.is_enabled = json.is_enabled;
  }

  private static validate = Ajv.compile(schema);
}

export = UpdateMissionEnabledPayload;
