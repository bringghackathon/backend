import * as ajv from 'ajv';
import * as fs from 'fs';
import {ErrorWithCode} from "../../../shared/ErrorWithCode";

const schema = JSON.parse(fs.readFileSync(__dirname + '/schemas/LoginModel.json').toString());
const Ajv = new ajv();

class LoginModel {

  /**
   * The user email
   *
   * @minLength 1
   */
  email: string;

  /**
   * The user password
   *
   * @minLength 1
   */
  password: string;

  constructor(json: Partial<LoginModel>) {
    const valid = LoginModel.validate(json);
    if (!valid) {
      throw new ErrorWithCode(Ajv.errorsText(LoginModel.validate.errors), 400);
    }

    this.email = json.email;
    this.password = json.password;
  }

  private static validate = Ajv.compile(schema);
}

export = LoginModel;
