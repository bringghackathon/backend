import * as ajv from 'ajv';
import * as fs from 'fs';
import {ErrorWithCode} from "../../../shared/ErrorWithCode";
import {MissionType} from "../../Enums/MissionType";

const schema = JSON.parse(fs.readFileSync(__dirname + '/schemas/UpdateMissionFulfillmentPayload.json').toString());
const Ajv = new ajv();

class UpdateMissionFulfillmentPayload {
  bringg_user_id: number;

  type: MissionType;

  additional_parts_completed: number;

  constructor(json: Partial<UpdateMissionFulfillmentPayload >) {
    const valid = UpdateMissionFulfillmentPayload .validate(json);
    if (!valid) {
      throw new ErrorWithCode(Ajv.errorsText(UpdateMissionFulfillmentPayload .validate.errors), 400);
    }

    this.bringg_user_id = json.bringg_user_id;
    this.type = json.type;
    this.additional_parts_completed = json.additional_parts_completed;
  }

  private static validate = Ajv.compile(schema);
}

export = UpdateMissionFulfillmentPayload ;
