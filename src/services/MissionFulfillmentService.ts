import * as express from 'express';
import {Request, Response, Router} from "express";
import {handleErrors} from "./ServiceUtils";
import {Mission} from "../entity/Mission";
import {User} from "../entity/User";
import {MissionFulfillment} from "../entity/MissionFulfillment";
import {ErrorWithCode} from "../../shared/ErrorWithCode";
import UpdateMissionFulfillmentPayload = require("./models/UpdateMissionFulfillmentPayload");
import {UserNotification} from "../entity/UserNotification";

export default class MissionFulfillmentService {
  private readonly router: Router;

  constructor() {
    const router = express.Router({mergeParams: true});

    router.post('/', handleErrors(this.updateMissionFulfillment.bind(this)));

    this.router = router;
  }

  private async updateMissionFulfillment(req: Request, res: Response) {
    const user = req['user'] as User;
    const updateMissionFulfillmentPayload = new UpdateMissionFulfillmentPayload(req.body);

    const mission = await Mission.findOne({type: updateMissionFulfillmentPayload.type});
    if (!mission) {
      throw new ErrorWithCode("No message with type", 404, 243653456, updateMissionFulfillmentPayload);
    }

    const missionFulfilmentToUpdate = await MissionFulfillment.getOrCreateMissionFulfillment(updateMissionFulfillmentPayload.bringg_user_id, mission.id);
    missionFulfilmentToUpdate.parts_completed += updateMissionFulfillmentPayload.additional_parts_completed;
    if (missionFulfilmentToUpdate.parts_completed > mission.achievement_parts) {
      missionFulfilmentToUpdate.parts_completed = mission.achievement_parts;
    }

    await missionFulfilmentToUpdate.save();

    const notification = UserNotification.createWith(missionFulfilmentToUpdate, mission, updateMissionFulfillmentPayload.additional_parts_completed);
    await notification.save();

    res.json({success: true});
  }

  getRoutes(): Router {
    return this.router;
  }
}