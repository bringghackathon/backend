export enum MissionType {
  join_rewards_club = "join_rewards_club",
  finish_order_in_time = "finish_order_in_time",
  time_on_site_less_than = "time_on_site_less_than",
  customer_rating_greater_than = "customer_rating_greater_than"
}