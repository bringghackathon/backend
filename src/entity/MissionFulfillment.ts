import {Entity, PrimaryGeneratedColumn, Column, OneToMany, BaseEntity, ManyToOne, Not} from "typeorm";
import * as _ from 'lodash';
import {UserNotification} from "./UserNotification";

@Entity()
export class MissionFulfillment extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  bringg_user_id: number;

  @Column()
  mission_id: string;

  @Column()
  parts_completed: number;

  @OneToMany(type => UserNotification, notification => notification.fulfilment)
  notifications: UserNotification[];

  private static createFromData(data: { bringg_user_id: number, mission_id: string }): MissionFulfillment {
    const missionFulfillment = new MissionFulfillment();

    missionFulfillment.bringg_user_id = data.bringg_user_id;
    missionFulfillment.mission_id = data.mission_id;
    missionFulfillment.parts_completed = 0;

    return missionFulfillment;
  }

  static async getOrCreateMissionFulfillment(bringgUserId: number, missionId: string): Promise<MissionFulfillment> {
    const missionFulfillment = await MissionFulfillment.findOne({bringg_user_id: bringgUserId, mission_id: missionId})
    if (missionFulfillment) {
      return missionFulfillment;
    }

    const createdMissionFulfillment = this.createFromData({bringg_user_id: bringgUserId, mission_id: missionId});
    await createdMissionFulfillment.save();

    return createdMissionFulfillment;
  }

  serialize(): _.PartialDeep<MissionFulfillment> {
    const serializedUser: any = _.pick(this, [
      'bringg_user_id',
      'parts_completed',
    ]);
    return serializedUser;
  }
}
