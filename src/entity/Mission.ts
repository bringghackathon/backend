import {Entity, PrimaryGeneratedColumn, Column, OneToMany, BaseEntity, ManyToOne, Not} from "typeorm";
import * as _ from 'lodash';
import CreateMissionPayload = require("../services/models/CreateMissionPayload");
import {MissionType} from "../Enums/MissionType";
import {UserNotification} from "./UserNotification";

@Entity()
export class Mission extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  title: string;

  @Column()
  achievement_parts: number;

  @Column()
  point_reward: number;

  @Column()
  is_enabled: boolean;

  @Column({enum: MissionType})
  type: MissionType;

  @Column({nullable: true})
  mission_parameter: number | null;

  @OneToMany(type => UserNotification, notification => notification.fulfilment)
  notifications: UserNotification[];

  static createFromData(data: CreateMissionPayload): Mission {
    const mission = new Mission();

    mission.title = data.title;
    mission.achievement_parts = data.achievement_parts;
    mission.point_reward = data.point_reward;
    mission.is_enabled = true;
    mission.type = data.mission_type;
    mission.mission_parameter = data.mission_parameter;

    return mission;
  }

  serialize(): _.PartialDeep<Mission> {
    const serializedUser: any = _.pick(this, [
      'id',
      'image_url',
      'title',
      'achievement_parts',
      'point_reward',
      'is_enabled',
      'type',
      'mission_parameter'
    ]);
    return serializedUser;
  }
}
