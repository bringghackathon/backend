import {Entity, PrimaryGeneratedColumn, Column, OneToMany, BaseEntity, ManyToOne, Not} from "typeorm";
import * as _ from 'lodash';

@Entity()
export class User extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  bringg_user_id: number;

  static createFromData(data: {bringgUserId: number}): User {
    const user = new User();
    user.bringg_user_id = data.bringgUserId;
    return user;
  }

  static async getOrCreateUserByBringgUserId(bringgUserId: number): Promise<User> {
    const foundUser = await User.findOne({bringg_user_id: bringgUserId});
    if (foundUser) {
      return foundUser;
    }

    const createdUser = this.createFromData({bringgUserId});
    await createdUser.save();
    return createdUser;
  }

  serialize(): _.PartialDeep<User> {
    const serializedUser: any = _.pick(this, [
      'id',
      'bringg_user_id',
    ]);
    return serializedUser;
  }
}
