import {Entity, PrimaryGeneratedColumn, Column, OneToMany, BaseEntity, ManyToOne, Not, OneToOne} from "typeorm";
import * as _ from 'lodash';
import {MissionFulfillment} from "./MissionFulfillment";
import {Mission} from "./Mission";

@Entity()
export class UserNotification extends BaseEntity {
  @PrimaryGeneratedColumn("uuid")
  id: string;

  @Column()
  bringg_user_id: number;

  @ManyToOne(type => MissionFulfillment, missionFulfillment => missionFulfillment.notifications)
  fulfilment: MissionFulfillment;

  @ManyToOne(type => Mission, mission => mission.notifications)
  mission: Mission;

  @Column()
  is_read: boolean;

  @Column()
  additional_parts_completed: number;

  static createWith(fulfillment: MissionFulfillment, mission: Mission, additional_parts_completed: number): UserNotification {
    const notification = new UserNotification();

    notification.bringg_user_id = fulfillment.bringg_user_id;
    notification.fulfilment = fulfillment;
    notification.mission = mission;
    notification.is_read = false;
    notification.additional_parts_completed = additional_parts_completed;

    return notification;
  }

  static findAllUnreadForUserId(bringgUserId: number): Promise<UserNotification[]> {
    return UserNotification.find({
      where: {
        bringg_user_id: bringgUserId,
        is_read: false
      },
      relations: ['fulfilment', 'mission']
    })
  }

  async markAsRead() {
    if (this.is_read) {
      return;
    }

    this.is_read = true;
    await this.save();
  }

  serialize(): _.PartialDeep<Notification> {
    const serializedUserNotification: any = _.pick(this, [
      'id',
      'bringg_user_id',
      'is_read',
      'additional_parts_completed'
    ]);

    serializedUserNotification['fulfilment'] = this.fulfilment && this.fulfilment.serialize();
    serializedUserNotification['mission'] = this.mission && this.mission.serialize();

    return serializedUserNotification;
  }
}
