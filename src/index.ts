import "reflect-metadata";
import * as express from 'express';
import * as bodyParser from 'body-parser';
import errorMiddleware from "./services/middleware/errorMidleware";
import * as config from "config";
import * as cors from 'cors';
import * as morgan from 'morgan';
import {createDBConnection} from "../shared/shared";
import * as http from 'http';
import {initializeLogger, logError, logInfo, winstonRequestLogger} from "../shared/Logger";
import {authenticateUser} from "./services/middleware/AuthMiddleware";
import MissionsService from "./services/MissionsService";
import MissionFulfillmentService from "./services/MissionFulfillmentService";
import UserService from "./services/UserService";
import RewardsService from "./services/RewardsService";
import NotificationsService from "./services/NotificationsService";

initializeLogger();

function runAPI(options: {
  port: number,
}) {
  const app = express();
  const server = http.createServer(app);

  const {
    port,
  } = options;

  app.use(winstonRequestLogger());
  app.use(morgan('short'));

  app.use(cors());
  app.use(bodyParser.json());
  app.use(bodyParser.urlencoded({extended: true}));

  app.get('/', (req, res) => {
    res.json({status: 200});
  });

  app.use('/api/missions', authenticateUser, new MissionsService().getRoutes());
  app.use('/api/rewards', new RewardsService().getRoutes());
  app.use('/api/fulfillments', authenticateUser, new MissionFulfillmentService().getRoutes());
  app.use('/api/notifications', authenticateUser, new NotificationsService().getRoutes());
  app.use('/api/me', authenticateUser, new UserService().getRoutes());

  app.use(errorMiddleware);

  server.listen(port);
}

async function main() {
  await createDBConnection("web-server");

  logInfo(`Starting the api. ENV: ${process.env.NODE_ENV}`);
  const port = (process.env.PORT || config.get('server.port')) as number;

  runAPI({
    port,
  });
  logInfo("Started server", {port});
}

main().catch(error => {
  logError(`Failed starting server "${error.message}"`, {error});
});
