import {PhoneNumberUtil, PhoneNumberFormat} from 'google-libphonenumber';

const phoneUtil = PhoneNumberUtil.getInstance();

export function formatPhoneNumber(phoneObject: { phoneNumber: string, countryCode: string }): string {
  const phoneNumberObject = phoneUtil.parse(phoneObject.phoneNumber, phoneObject.countryCode);
  return phoneUtil.format(phoneNumberObject, PhoneNumberFormat.E164);
}