import * as moment from 'moment';
import {Moment} from "moment";

export function now(): Moment {
  return moment();
}