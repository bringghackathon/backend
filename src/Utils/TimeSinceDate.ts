import {now} from "./now";

export function hoursSinceDate(date: Date): number {
  return now().diff(date, 'hours', true);
}