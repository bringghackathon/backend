import * as moment from 'moment';

export function mapObjectKeysAndValues(object: { [key: string]: any }, mapper: (key: string, value: any) => any): { [key: string]: any} {
  const mappedObject = {};

  for (const key in object) {
    const value = object[key];
    mappedObject[key] = mapper(key, value);
  }

  return mappedObject;
}

