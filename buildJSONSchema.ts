import {resolve} from "path";
import * as glob from 'glob';
import * as TJS from "typescript-json-schema";
import * as fs from "fs";
import * as mkdirp from 'mkdirp';

const folderNames = [
  'src/services/models'
];

const settings: TJS.PartialArgs = {
  required: true
};

const compilerOptions: TJS.CompilerOptions = {};

folderNames.forEach((folderName) => {
  const fileNames = glob.sync(`${folderName}/*.ts`)
  const filePaths = fileNames.map((file) => {
    return resolve(file);
  });

  const program = TJS.getProgramFromFiles(filePaths, compilerOptions);
  const generator = TJS.buildGenerator(program, settings);
  const symbols = generator.getMainFileSymbols(program);

  symbols.forEach((symbol) => {
    const schema = generator.getSchemaForSymbol(symbol);
    const schemaString = JSON.stringify(schema, null, 2);
    const schemaFileName = resolve(folderName, "schemas", `${symbol}.json`);
    fs.writeFileSync(schemaFileName, schemaString);

    const buildschemaFolder = resolve("build", folderName, "schemas");
    mkdirp.sync(buildschemaFolder);

    const buildschemaFileName = resolve("build", folderName, "schemas", `${symbol}.json`);
    fs.writeFileSync(buildschemaFileName, schemaString);
  });
});
