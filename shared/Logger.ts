import * as winston from 'winston';
import {Logger} from "winston";
import {RequestHandler} from "express";
import * as expressWinston from 'express-winston';

let logger: Logger;

export function logInfo(message: string, meta: any | undefined = undefined) {
  logger.info(message, meta);
}

export function logWarn(message: string, meta: any | undefined = undefined) {
  logger.warn(message, meta);
}

export function logError(message: string, meta: any | undefined = undefined) {
  logger.error(message, meta);
}

function transports() {
  const transports: any = [
    new winston.transports.Console({
      format: winston.format.simple()
    })
  ];

  return transports;
}

export function initializeLogger() {
  logger = winston.createLogger({
    level: 'info',
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.json()
    ),
    transports: transports()
  });
}

export function winstonRequestLogger(): RequestHandler {
  return expressWinston.logger({
    expressFormat: true,
    colorize: true,
    transports: transports(),
    requestWhitelist: ['url', 'originalUrl', 'query', 'body'],
    bodyBlacklist: ['password'],
    dynamicMeta: (req, res) => {
      return {
        loggedInUsername: req['user'] && req['user'].username
      }
    }
  });
}
