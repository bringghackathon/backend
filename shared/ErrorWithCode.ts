export class ErrorWithCode extends Error {
  httpCode: number;
  errorCode?: number;
  logMeta: any | undefined;

  constructor(message: string, httpCode: number, errorCode: number | undefined = undefined, logMeta: any | undefined = undefined) {
    super(message);
    this.httpCode = httpCode;
    this.errorCode = errorCode;
    this.logMeta = logMeta;
  }
}