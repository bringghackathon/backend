import {ConnectionOptions, createConnection, getConnectionOptions} from "typeorm";
import * as config from "config";
import {logInfo} from "./Logger";

export function dbConnectionOptions(connectionOptions: ConnectionOptions): ConnectionOptions {
  if (process.env.DATABASE_URL) {
    logInfo("Using database connection url from ENV");
    return Object.assign(connectionOptions, {url: process.env.DATABASE_URL})
  } else {
    logInfo("Using database connection url from config");
    return Object.assign(connectionOptions, config.get('database'));
  }
}

export async function createDBConnection(context: string) {
  logInfo(`Creating the connection. ${context}`);
  const connectionOptions = await getConnectionOptions();
  const connectionOptionsWithCredentials = dbConnectionOptions(connectionOptions);
  await createConnection(connectionOptionsWithCredentials);
  logInfo(`Created the connection ${context}`);
}
