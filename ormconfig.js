var config = {
  "type": "postgres",
  "host": "localhost",
  "synchronize": true,
  "logging": false,
  "entities": [
    "build/src/entity/**/*.js"
  ],
  "migrations": [
    "build/src/migration/**/*.js"
  ],
  "subscribers": [
    "build/src/subscriber/**/*.js"
  ],
  "cli": {
    "entitiesDir": "src/entity",
    "migrationsDir": "src/migration",
    "subscribersDir": "src/subscriber"
  }
};

if (process.env.USE_TS_NODE_VERSION) {
  console.log("using ts node version");
  config.entities = [
    "src/entity/**/*.ts"
  ];
  config.migrations = [
    "src/migration/**/*.ts"
  ];
  config.subscribers = [
    "src/subscriber/**/*.ts"
  ];
}

module.exports = config;